<?php

/* Do we need to run the installer? */
if (!file_exists('INSTALL_BLOCK') && !isset($_POST['performMaintenence']))
{
    include('modules/install/notinstalled.php');
    die();
}

// FIXME: Config file setting.
@ini_set('memory_limit', '64M');

/* Hack to make ELECTIONS work with E_STRICT. */
if (function_exists('date_default_timezone_set'))
{
    @date_default_timezone_set(date_default_timezone_get());
}

include_once('./config.php');
include_once('./constants.php');
include_once('./lib/CommonErrors.php');
include_once('./lib/CATSUtility.php');
include_once('./lib/DatabaseConnection.php');
include_once('./lib/Template.php');
include_once('./lib/Users.php');
include_once('./lib/MRU.php');
include_once('./lib/Hooks.php');
include_once('./lib/Session.php'); /* Depends: MRU, Users, DatabaseConnection. */
include_once('./lib/UserInterface.php'); /* Depends: Template, Session. */
include_once('./lib/ModuleUtility.php'); /* Depends: UserInterface */
include_once('./lib/TemplateUtility.php'); /* Depends: ModuleUtility, Hooks */


/* Give the session a unique name to avoid conflicts and start the session. */
@session_name(ELECTIONS_SESSION_NAME);
session_start();

/* Try to prevent caching. */
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');

// This function assures to strip the values from
// request arrays even if as values are arrays not only values
function stripslashes_deep($value)
{
    $value = is_array($value) ?
                array_map('stripslashes_deep', $value) :
                stripslashes($value);

    return $value;
}

/* Make sure we aren't getting screwed over by magic quotes. */
if (get_magic_quotes_runtime())
{
    set_magic_quotes_runtime(0);
}
if (get_magic_quotes_gpc())
{
    include_once('./lib/ArrayUtility.php');

    $_GET     = array_map('stripslashes_deep', $_GET);
    $_POST    = array_map('stripslashes_deep', $_POST);
    $_REQUEST = array_map('stripslashes_deep', $_REQUEST);
    $_GET     = ArrayUtility::arrayMapKeys('stripslashes_deep', $_GET);
    $_POST    = ArrayUtility::arrayMapKeys('stripslashes_deep', $_POST);
    $_REQUEST = ArrayUtility::arrayMapKeys('stripslashes_deep', $_REQUEST);
}

/* Objects can't be stored in the session if session.auto_start is enabled. */
if (ini_get('session.auto_start') !== '0' &&
    ini_get('session.auto_start') !== 'Off')
{
    die('ELECTIONS Error: session.auto_start must be set to 0 in php.ini.');
}

/* Proper extensions loaded?! */
if (!function_exists('mysql_connect') || !function_exists('session_start'))
{
    die('ELECTIONS Error: All required PHP extensions are not loaded.');
}

/* Make sure we have a Session object stored in the user's session. */
if (!isset($_SESSION['ELECTIONS']) || empty($_SESSION['ELECTIONS']))
{
    $_SESSION['ELECTIONS'] = new CATSSession();
}

/* Start timer for measuring server response time. Displayed in footer. */
$_SESSION['CATS']->startTimer();

/* Check to see if the server went through a SVN update while the session
 * was active.
 */
$_SESSION['ELECTIONS']->checkForcedUpdate();

/* Check to see if the user level suddenly changed. If the user was changed to disabled,
 * also log the user out.
 */
// FIXME: This is slow!
if ($_SESSION['ELECTIONS']->isLoggedIn())
{
    $users = new Users($_SESSION['CATS']->getSiteID());
    $forceLogoutData = $users->getForceLogoutData($_SESSION['ELECTIONS']->getUserID());

    if (!empty($forceLogoutData) && ($forceLogoutData['forceLogout'] == 1 ||
        $_SESSION['ELECTIONS']->getRealAccessLevel() != $forceLogoutData['accessLevel']))
    {
        $_SESSION['ELECTIONS']->setRealAccessLevel($forceLogoutData['accessLevel']);

        if ($forceLogoutData['accessLevel'] == ACCESS_LEVEL_DISABLED ||
            $forceLogoutData['forceLogout'] == 1)
        {
            /* Log the user out. */
            $unixName = $_SESSION['CATS']->getUnixName();

            $_SESSION['ELECTIONS']->logout();
            unset($_SESSION['ELECTIONS']);
            unset($_SESSION['modules']);

            $URI = 'm=login';

            if (!empty($unixName) && $unixName != 'demo')
            {
                $URI .= '&s=' . $unixName;
            }

            ELECTIONSUtility::transferRelativeURI($URI);
            die();
        }
    }
}

/* Check to see if we are supposed to display the career page. */
if (((isset($careerPage) && $careerPage) ||
    (isset($_GET['showCareerPortal']) && $_GET['showCareerPortal'] == '1')))
{
    ModuleUtility::loadModule('careers');
}

/* Check to see if we are supposed to display an rss page. */
else if (isset($rssPage) && $rssPage)
{
    ModuleUtility::loadModule('rss');
}

else if (isset($xmlPage) && $xmlPage)
{
    ModuleUtility::loadModule('xml');
}

/* Check to see if the user was forcibly logged out (logged in from another browser). */
else if ($_SESSION['ELECTIONS']->isLoggedIn() &&
    (!isset($_GET['m']) || ModuleUtility::moduleRequiresAuthentication($_GET['m'])) &&
    $_SESSION['ELECTIONS']->checkForceLogout())
{
    // FIXME: Unset session / etc.?
    ModuleUtility::loadModule('login');
}

/* If user specified a module, load it; otherwise, load the home module. */
else if (!isset($_GET['m']) || empty($_GET['m']))
{
    if ($_SESSION['ELECTIONS']->isLoggedIn())
    {
        $_SESSION['ELECTIONS']->logPageView();

        if (!eval(Hooks::get('INDEX_LOAD_HOME'))) return;

        ModuleUtility::loadModule('home');
    }
    else
    {
        ModuleUtility::loadModule('login');
    }
}
else
{
    if ($_GET['m'] == 'logout')
    {
        /* There isn't really a logout module. It's just a few lines. */
        $unixName = $_SESSION['CATS']->getUnixName();

        $_SESSION['ELECTIONS']->logout();
        unset($_SESSION['ELECTIONS']);
        unset($_SESSION['modules']);

        $URI = 'm=login';
                                 /* Local demo account doesn't relogin. */
        if (!empty($unixName) && $unixName != 'demo')
        {
            $URI .= '&s=' . $unixName;
        }

        if (isset($_GET['message']))
        {
            $URI .= '&message=' . urlencode($_GET['message']);
        }

        if (isset($_GET['messageSuccess']))
        {
            $URI .= '&messageSuccess=' . urlencode($_GET['messageSuccess']);
        }

        /* catsone.com demo domain doesn't relogin. */
        if (strpos(ELECTIONSUtility::getIndexName(), '://demo.kingdoms.network') !== false)
        {
            ELECTIONSUtility::transferURL('http://home.kingdoms.network');
        }
        else
        {
            ELECTIONSSUtility::transferRelativeURI($URI);
        }
    }
    else if (!ModuleUtility::moduleRequiresAuthentication($_GET['m']))
    {
        /* No authentication required; load the module. */
        ModuleUtility::loadModule($_GET['m']);
    }
    else if (!$_SESSION['ELECTIONS']->isLoggedIn())
    {
        /* User isn't logged in and authentication is required; send the user
         * to the login page.
         */
        ModuleUtility::loadModule('login');
    }
    else
    {
        /* Everything's good; load the requested module. */
        $_SESSION['ELECTIONS']->logPageView();
        ModuleUtility::loadModule($_GET['m']);
    }
}

?>
